#include "game_rules.hpp"
#include "food_spawner.hpp"
#include "player.hpp"

#include <random>

#include <scene.hpp>
#include <event_system.hpp>

GameRules::GameRules(const PlayerPtr& player, const FoodSpawnerPtr& spawner, const ScenePtr& scene) :
    TE::Object(0, 0, 0, 0),
    m_player(player),
    m_spawner(spawner),
    m_scene(scene)
{
    m_score = 0;
    m_over = false;

    m_player->getEventSystem()->appendSubscriber(this);
    m_spawner->getEventSystem()->appendSubscriber(this);
}

bool GameRules::isGameOver() const {
    return m_over;
}

int GameRules::getScore() const {
    return m_score;
}

void GameRules::onDestroy() {}
    
void GameRules::onUpdate(clock_t FPS) {}

void GameRules::onDraw(const CanvasPtr& canvas) {}

void GameRules::onIntersects(const ObjectPtr& object) {}

void GameRules::onEvent(const TE::EventPtr& event) {
    if (event->type == TE::EventType::Created) {
        event->object->getEventSystem()->appendSubscriber(this);
    } else if (event->type == TE::EventType::Destroyed) {
        if (event->object->hasTag("food")) {
            auto element = std::make_shared<SnakeElement>(4, 2, -100, -100);

            m_scene->appendObject(element);
            m_player->appendElement(element);

            m_score += 100;
        } else if (event->object->hasTag("player")) {
            m_over = true;
        }
    }
}