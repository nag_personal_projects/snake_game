#ifndef SNAKE_ELEMENT_HPP
#define SNAKE_ELEMENT_HPP

#include <memory>

#include <object.hpp>

namespace TE {
    class Canvas;
};

typedef std::shared_ptr<TE::Object> ObjectPtr;
typedef std::shared_ptr<TE::Canvas> CanvasPtr;

class SnakeElement : public TE::Object {

public:

    SnakeElement(int w, int h, int x, int y);
    ~SnakeElement() override = default;

protected:

    virtual void onDestroy() override;
    virtual void onUpdate(clock_t FPS) override;
    virtual void onDraw(const CanvasPtr& canvas) override;
    virtual void onIntersects(const ObjectPtr& object) override;

};

#endif //!SNAKE_ELEMENT_HPP