#ifndef GAME_FIELD_HPP
#define GAME_FIELD_HPP

#include <memory>

#include <object.hpp>

namespace TE
{
    class Canvas;
}

typedef std::shared_ptr<TE::Object> ObjectPtr;
typedef std::shared_ptr<TE::Canvas> CanvasPtr;

class GameField : public TE::Object {

    int m_widthStep;
    int m_heightStep;

public:

    GameField(int w, int h, int ws, int hs);
    ~GameField() override = default;

protected:

    void onDestroy() override;
    void onUpdate(clock_t FPS) override;
    void onDraw(const CanvasPtr& canvas) override;
    void onIntersects(const ObjectPtr& object) override;

};

#endif //!GAME_FIELD_HPP