#include "food_spawner.hpp"
#include "food.hpp"

#include <ctime>
#include <random>

#include <scene.hpp>
#include <event_system.hpp>

FoodSpawner::FoodSpawner(ScenePtr_w scene, int w, int h, int ws, int hs) :
    TE::Object(0, 0, 0, 0),
    m_scene(scene)
{
    m_isWorking = false;

    auto s = scene.lock();
    initSpawner(s->getWidth(), s->getHeight(), ws, hs);
}

void FoodSpawner::start() {
    if (!m_isWorking) {
        m_isWorking = true;
    }

    spawn();
}

void FoodSpawner::stop() {
    if (m_isWorking) {
        m_isWorking = false;
    }
}

void FoodSpawner::onDestroy() {}

void FoodSpawner::onUpdate(clock_t FPS) {}

void FoodSpawner::onDraw(const CanvasPtr& canvas) {}

void FoodSpawner::onIntersects(const ObjectPtr& object) {}

void FoodSpawner::onEvent(const EventPtr& event) {
    if (m_isWorking &&
        event->type == TE::EventType::Destroyed &&
        event->object->hasTag("food"))
    {
        spawn();
    }
}

void FoodSpawner::initSpawner(int w, int h, int ws, int hs) {
    for (int i = 1; i < w; i += ws + 1) {
        m_WidthValidIndices.push_back(i);
    }

    for (int i = 1; i < h; i += hs + 1) {
        m_HeightValidIndices.push_back(i);
    }
}

void FoodSpawner::spawn() {
    const int x = m_WidthValidIndices[rand() % m_WidthValidIndices.size()];
    const int y = m_HeightValidIndices[rand() % m_HeightValidIndices.size()];

    ObjectPtr food = std::make_shared<Food>(4, 2, x, y);
    m_scene.lock()->appendObject(food);

    food->getEventSystem()->appendSubscriber(this);

    auto event = std::make_shared<TE::Event>(this, food.get(), TE::EventType::Created);
    getEventSystem()->send(event);
}