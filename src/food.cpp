#include "food.hpp"

#include <animation_system/animation_component.hpp>
#include <animation_system/animation.hpp>
#include <image.hpp>

#include <canvas.hpp>

Food::Food(int w, int h,
           int x, int y) :
    TE::Object(w, h, x, y)
{
    appendTag("food");

    auto frame_1 = TE::Image::createFromFile("./../resources/Food_Idle/1.imgte");
    auto frame_2 = TE::Image::createFromFile("./../resources/Food_Idle/2.imgte");

    auto idleAnim = std::make_shared<TE::Animation>("FoodIdle", 1);
    idleAnim->appendFrame(frame_1);
    idleAnim->appendFrame(frame_2);
    idleAnim->setLooped(true);

    auto animComponent = appendComponent<TE::AnimationComponent>();
    animComponent->appendAnimation(idleAnim);
    animComponent->play();

    m_animComponent = animComponent;
}

void Food::onDestroy() {}

void Food::onUpdate(clock_t FPS) {}

void Food::onDraw(const CanvasPtr& canvas) {
    auto texture = getTexture();
    if (!texture) {
        return;
    }

    for (int i = m_posX, x = 0; i < m_width + m_posX; ++i, ++x) {
        for (int j = m_posY, y = 0; j < m_height + m_posY; ++j, ++y) {
            canvas->at(i, j) = texture->at(x, y);
        }
    }
}

void Food::onIntersects(const ObjectPtr& object) {
    if (object->hasTag("player")) {
        destroy();
    }
}