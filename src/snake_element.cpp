#include "snake_element.hpp"

#include <canvas.hpp>

SnakeElement::SnakeElement(int w, int h, int x, int y) :
    TE::Object(w, h, x, y)
{
    appendTag("element");
}

void SnakeElement::onDestroy() {}

void SnakeElement::onUpdate(clock_t FPS) {}

void SnakeElement::onDraw(const CanvasPtr& canvas) {
    for (int i = m_posX; i < m_width + m_posX; ++i) {
        for (int j = m_posY; j < m_height + m_posY; ++j) {
            canvas->at(i, j) = '*';
        }
    }
}

void SnakeElement::onIntersects(const ObjectPtr& object) {}