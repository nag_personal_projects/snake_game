#include <iostream>
#include <memory>

#include <engine.hpp>
#include <scene.hpp>
#include <object.hpp>
#include <keyboard.hpp>

#include "game_field.hpp"
#include "player.hpp"

#include "game_rules.hpp"
#include "food_spawner.hpp"

typedef std::shared_ptr<TE::Engine> EnginePtr;
typedef std::shared_ptr<TE::Scene> ScenePtr;
typedef std::shared_ptr<TE::Object> ObjectPtr;

typedef std::shared_ptr<Player> PlayerPtr;
typedef std::shared_ptr<FoodSpawner> FoodSpawnerPtr;
typedef std::shared_ptr<GameRules> GameRulesPtr;

int main() {
    srand(time(nullptr));

    ScenePtr scene = std::make_shared<TE::Scene>(101, 49);
    FoodSpawnerPtr spawner = std::make_shared<FoodSpawner>(scene, 101, 49, 4, 2);

    ObjectPtr gameField = std::make_shared<GameField>(101, 49, 4, 2);
    scene->appendObject(gameField);

    PlayerPtr player = std::make_shared<Player>(4, 2, 1, 1, 4, 2);
    scene->appendObject(player);

    GameRulesPtr rules = std::make_shared<GameRules>(player, spawner, scene);

    EnginePtr engine = std::make_shared<TE::Engine>("Snake", scene);
    engine->setLimitFPS(10);
    engine->limitFPS(true);

    engine->run();
    spawner->start();

    while(!TE::Keyboard::isKeyPressed(XK_Escape) && !rules->isGameOver());

    engine->stop();
    spawner->stop();

    std::cout << "your score is " << rules->getScore() << std::endl;
    if (rules->isGameOver()) {
        std::cout << "game over" << std::endl;
    }

    //***********************************************************

    // auto engine = std::make_shared<TE::Engine>("keylistener", nullptr);

    // engine->run();

    // while(!TE::Keyboard::isKeyPressed(XK_Escape));

    // engine->stop();
    
    //***********************************************************

    return 0;
}