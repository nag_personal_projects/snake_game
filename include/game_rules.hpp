#ifndef GAME_RULES_HPP
#define GAME_RULES_HPP

#include <memory>

#include <object.hpp>

namespace TE {
    class Event;
    class Canvas;
    class Scene;
};

typedef std::shared_ptr<TE::Object> ObjectPtr;
typedef std::shared_ptr<TE::Canvas> CanvasPtr;
typedef std::shared_ptr<TE::Scene> ScenePtr;

class Player;
typedef std::shared_ptr<Player> PlayerPtr;

class FoodSpawner;
typedef std::shared_ptr<FoodSpawner> FoodSpawnerPtr;

// Don't add an object of this class to the scene !!!
class GameRules : public TE::Object {

    int m_score;

    bool m_over;

    PlayerPtr m_player;
    FoodSpawnerPtr m_spawner;
    ScenePtr m_scene;

public:

    GameRules(const PlayerPtr& player, const FoodSpawnerPtr& spawner, const ScenePtr& scene);
    ~GameRules() override = default;

    bool isGameOver() const;

    int getScore() const;

protected:

    void onDestroy() override;
    
    void onUpdate(clock_t FPS) override;
    void onDraw(const CanvasPtr& canvas) override;
    void onIntersects(const ObjectPtr& object) override;

    void onEvent(const TE::EventPtr& event) override;

};

#endif //!GAME_RULES_HPP