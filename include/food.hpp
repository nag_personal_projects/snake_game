#ifndef FOOD_HPP
#define FOOD_HPP

#include <memory>

#include <object.hpp>

namespace TE {
    class Canvas;
    class AnimationComponent;
};

typedef std::shared_ptr<TE::Object> ObjectPtr;
typedef std::shared_ptr<TE::Canvas> CanvasPtr;

typedef std::shared_ptr<TE::AnimationComponent> AnimationComponentPtr;
typedef std::weak_ptr<TE::AnimationComponent> AnimationComponentPtr_w;

class Food : public TE::Object {

    AnimationComponentPtr_w m_animComponent;

public:

    Food(int w, int h, int x, int y);
    ~Food() override = default;

protected:

    void onDestroy() override;

    void onUpdate(clock_t FPS) override;
    void onDraw(const CanvasPtr& canvas) override;
    void onIntersects(const ObjectPtr& object) override;

};

#endif //!FOOD_HPP