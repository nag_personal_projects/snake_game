#ifndef FOOD_SPAWNER_HPP
#define FOOD_SPAWNER_HPP

#include <memory>

#include <object.hpp>

namespace TE {
    class Canvas;
    class Scene;
    class Event;
};

typedef std::shared_ptr<TE::Object> ObjectPtr;
typedef std::shared_ptr<TE::Canvas> CanvasPtr;
typedef std::shared_ptr<TE::Event> EventPtr;

typedef std::weak_ptr<TE::Scene> ScenePtr_w;

// Don't add an object of this class to the scene !!!
class FoodSpawner : public TE::Object {

    ScenePtr_w m_scene;

    std::vector<int> m_WidthValidIndices;
    std::vector<int> m_HeightValidIndices;

    bool m_isWorking;

public:

    FoodSpawner(ScenePtr_w scene, int w, int h, int ws, int hs);
    ~FoodSpawner() override = default;

    void start();
    void stop();

protected:

    void onDestroy() override;

    void onUpdate(clock_t FPS) override;
    void onDraw(const CanvasPtr& canvas) override;
    void onIntersects(const ObjectPtr& object) override;

    void onEvent(const EventPtr& event) override;

private:

    void initSpawner(int w, int h, int ws, int hs);
    void spawn();

};

#endif //!FOOD_SPAWNER_HPP