#include "player.hpp"

#include <animation_system/animation_component.hpp>
#include <animation_system/animation.hpp>
#include <image.hpp>

#include <keyboard.hpp>
#include <utils.hpp>
#include <canvas.hpp>

#include <iostream>

Player::Player(int w, int h,
               int x, int y,
               int ws, int hs) :
    SnakeElement(w, h, x, y)
{
    m_widthStep = ws;
    m_heightStep = hs;

    m_direction = PlayerDirection::RIGHT;

    auto frame_1 = TE::Image::createFromFile("./../resources/Head_Idle/1.imgte");
    auto frame_2 = TE::Image::createFromFile("./../resources/Head_Idle/2.imgte");

    auto idleAnim = std::make_shared<TE::Animation>("HeadIdle", 1);
    idleAnim->appendFrame(frame_1);
    idleAnim->appendFrame(frame_2);
    idleAnim->setLooped(true);

    m_animComponent = appendComponent<TE::AnimationComponent>();
    m_animComponent->appendAnimation(idleAnim);
    m_animComponent->play();

    removeTag("element");
    appendTag("player");
}

void Player::onDestroy() {
    for (auto element : m_elements) {
        element.lock()->destroy();
    }
}
void Player::onUpdate(clock_t FPS) {
    if (TE::Keyboard::isKeyPressed(XK_Up) && m_direction != PlayerDirection::DOWN) {
        m_direction = PlayerDirection::UP;
    } else if (TE::Keyboard::isKeyPressed(XK_Down) && m_direction != PlayerDirection::UP) {
        m_direction = PlayerDirection::DOWN;
    } else if (TE::Keyboard::isKeyPressed(XK_Left) && m_direction != PlayerDirection::RIGHT) {
        m_direction = PlayerDirection::LEFT;
    } else if (TE::Keyboard::isKeyPressed(XK_Right) && m_direction != PlayerDirection::LEFT) {
        m_direction = PlayerDirection::RIGHT;
    }

    move();
}

void Player::onDraw(const CanvasPtr& canvas) {
    auto texture = getTexture();

    for (int i = m_posX, x = 0; i < m_width + m_posX; ++i, ++x) {
        for (int j = m_posY, y = 0; j < m_height + m_posY; ++j, ++y) {
            canvas->at(i, j) = texture->at(x, y);
        }
    }
}

void Player::onIntersects(const ObjectPtr& object) {
    if (object->hasTag("element")) {
        destroy();
    }
}

void Player::move() {
    const int prevX = m_posX;
    const int prevY = m_posY;

    switch (m_direction)
    {
        case PlayerDirection::UP: {
            if (TE::Utils::compare(m_posY, 1)) {
                m_posY = 46;
            } else {
                m_posY -= m_heightStep + 1;
            }

            break;
        }
        case PlayerDirection::DOWN: {
            if (TE::Utils::compare(m_posY, 46)) {
                m_posY = 1;
            } else {
                m_posY += m_heightStep + 1;
            }
            break;
        }
        case PlayerDirection::LEFT: {
            if (TE::Utils::compare(m_posX, 1)) {
                m_posX = 96;
            } else {
                m_posX -= m_widthStep + 1;
            }

            break;
        }
        case PlayerDirection::RIGHT: {
            if (TE::Utils::compare(m_posX + 4, 100)) {
                m_posX = 1;
            } else {
                m_posX += m_widthStep + 1;
            }

            break;
        }
    }

    for (int i = m_elements.size() - 1; i > 0; --i) {
        SnakeElementPtr_w current_w = m_elements[i];
        SnakeElementPtr_w prev_w = m_elements[i - 1];

        SnakeElementPtr current = current_w.lock();
        SnakeElementPtr prev = prev_w.lock();

        const int prevX = prev->getPosX();
        const int prevY = prev->getPosY();

        current->setPosX(prevX);
        current->setPosY(prevY);
    }

    if (m_elements.empty() == false) {
        SnakeElementPtr first = (*m_elements.begin()).lock();

        first->setPosX(prevX);
        first->setPosY(prevY);
    }
}

void Player::appendElement(const SnakeElementPtr_w& element) {
    m_elements.push_back(element);
}