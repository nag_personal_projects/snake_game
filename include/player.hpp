#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "snake_element.hpp"

#include <vector>

namespace TE {
    class Scene;
    class AnimationComponent;
};

typedef std::shared_ptr<TE::Scene> ScenePtr;
typedef std::shared_ptr<TE::AnimationComponent> AnimationComponetPtr;

typedef std::shared_ptr<SnakeElement> SnakeElementPtr;
typedef std::weak_ptr<SnakeElement> SnakeElementPtr_w;

class Player : public SnakeElement {

    friend class GameRules;

    enum class PlayerDirection : char
    {
        UP = 0,
        DOWN = 1,
        LEFT = 2,
        RIGHT = 3
    } m_direction;

    int m_widthStep;
    int m_heightStep;

    std::vector<SnakeElementPtr_w> m_elements;

    AnimationComponetPtr m_animComponent;

public:

    Player(int w, int h, int x, int y, int ws, int hs);
    ~Player() override = default;

protected:

    void onDestroy() override;
    void onUpdate(clock_t FPS) override;
    void onDraw(const CanvasPtr& canvas) override;
    void onIntersects(const ObjectPtr& object) override;

private:

    void move();
    void appendElement(const SnakeElementPtr_w& element);

};

#endif //!PLAYER_HPP