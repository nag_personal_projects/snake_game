#include "game_field.hpp"

#include <canvas.hpp>

GameField::GameField(int w, int h, int ws, int hs) :
    TE::Object(w, h, 0, 0)
{
    m_widthStep = ws;
    m_heightStep = hs;
}

void GameField::onDestroy() {}

void GameField::onUpdate(clock_t FPS) {}

void GameField::onDraw(const CanvasPtr& canvas) {
    for (int i = 0; i < m_width; ++i) {
        for (int j = 0; j < m_height; ++j) {
            if (i % (m_widthStep + 1) == 0 || j % (m_heightStep + 1) == 0) {
                canvas->at(i, j) = '^';
            }
        }
    }
}

void GameField::onIntersects(const ObjectPtr& object) {}